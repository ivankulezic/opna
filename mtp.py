
from sympy import Poly, Symbol, sympify, Interval
from sympy.parsing.sympy_parser import parse_expr
from sympy.solvers.inequalities import reduce_rational_inequalities
from math import factorial, pi



def getSin(n):
    s = ""
    for i in range(0, (n-1)/2 + 1):
        s = s + "(-1) ** " + str(i) + "*x**(2*" + str(i) + "+1)/" + str(factorial(2*i+1)) + " + "
    return s[:-2]

def getCos(n):
    s = ""
    for i in range(0, n/2 + 1):
        s = s + "(-1) ** " + str(i) + "*x**(2*" + str(i) + ")/" + str(factorial(2*i)) + " + "
    return s[:-2]

def calc(n1, n2):
    expr = "1/3*x**3 +5*({sin}) - x*({cos}) > 0".format(sin=getSin(n1), cos=getCos(n2))
    s = "reduce_rational_inequalities([["+expr+"]], x)"
    return eval(s)

x = Symbol('x', real=True)

for i in range(1, 10):
    for j in range(0, 10):
        print i
        print j
        print calc(i, j)
        print "\n"

