package application;

import java.math.BigDecimal;
import java.math.BigInteger;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import rs.ac.bg.etf.math.RationalApproximationsCalculator;

public class Controller {

	@FXML
	private TextField constant;

	@FXML
	private Button bttnCalculate;

	@FXML
	private TextArea txtFirstOrder;

	@FXML
	private TextArea txtSecondOrder;

	@FXML
	private TextArea txtOther;
	
	@FXML
	private TextField m;
	
	@FXML
	private TextField n;

	@FXML
	private void calculate() {
		if(constant.getText().isEmpty() || m.getText().isEmpty() || n.getText().isEmpty()) {
			Alert alert = new Alert(AlertType.ERROR, "Potrebno je popuniti sva polja!", ButtonType.OK);
			alert.setTitle("Greska");
			alert.setHeaderText("Doslo je do greske.");
			alert.showAndWait();
			return;
		}
		RationalApproximationsCalculator rationalApproxCalc = new RationalApproximationsCalculator();
		rationalApproxCalc.compute(new BigDecimal(constant.getText()), new BigInteger(m.getText()), new BigInteger(n.getText()));
		txtFirstOrder.setText(rationalApproxCalc.getFirstOrderApproximations());
		txtSecondOrder.setText(rationalApproxCalc.getSecondOrderApproximations());
		txtOther.setText(rationalApproxCalc.getOtherApproximations());
	}

	@FXML
	private void closeApp() {
		Platform.exit();
	}
}
