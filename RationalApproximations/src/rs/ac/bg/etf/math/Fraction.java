package rs.ac.bg.etf.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class Fraction {
    private BigInteger numerator;
    private BigInteger denominator;
    private BigDecimal firstOrderError;
    private BigDecimal secondOrderError;

    public Fraction(final BigInteger numerator, final BigInteger denominator) {
        if (denominator.compareTo(BigInteger.ZERO) == 0) {
            throw new IllegalArgumentException("Denominator is zero.");
        }
        this.numerator = numerator;
        this.denominator = denominator;
        if (numerator.compareTo(BigInteger.ZERO) == 0) {
            this.denominator = BigInteger.ONE;
        }
    }

    public BigInteger getDenominator() {
        return denominator;
    }

    public BigInteger getNumerator() {
        return numerator;
    }

    @Override
    public String toString() {
        return numerator.toString() + "/" + denominator.toString();
    }

    public BigDecimal realValue() {
        return new BigDecimal(numerator).divide(new BigDecimal(denominator), 20, RoundingMode.HALF_EVEN);
    }

    public void setFirstOrderError(final BigDecimal original) {
        firstOrderError = original.subtract(realValue()).abs();
    }

    public BigDecimal getFirstOrderError() {
        return firstOrderError;
    }

    public void setSecondOrderError(final BigDecimal original) {
        secondOrderError = original.multiply(new BigDecimal(denominator.toString()))
                .subtract(new BigDecimal(numerator.toString())).abs();
    }

    public BigDecimal getSecondOrderError() {
        return secondOrderError;
    }

    public boolean isEqual(final Fraction f) {
        if (f.getDenominator().compareTo(denominator) == 0 && f.getNumerator().compareTo(numerator) == 0) {
            return true;
        }
        return false;
    }
}
