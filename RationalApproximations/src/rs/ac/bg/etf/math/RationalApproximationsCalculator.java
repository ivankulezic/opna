package rs.ac.bg.etf.math;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RationalApproximationsCalculator {
	private List<Fraction> firstOrder;

	private List<Fraction> secondOrder;

	private List<Fraction> other;

	public void compute(BigDecimal x, BigInteger m, BigInteger n) {
		other = new ArrayList<>();
		firstOrder = new ArrayList<>();

		for (BigInteger q = BigInteger.ONE; q.compareTo(m) <= 0; q = new BigInteger(q.toString()).add(BigInteger.ONE)) {
			BigInteger p = x.multiply(new BigDecimal(q.toString())).setScale(0, BigDecimal.ROUND_HALF_UP)
					.toBigInteger();
			Fraction f = new Fraction(p, q);
			f.setFirstOrderError(x);
			f.setSecondOrderError(x);
			boolean isFirstOrder = true;
			for (Fraction fraction : firstOrder) {
				if (fraction.getFirstOrderError().compareTo(f.getFirstOrderError()) <= 0) {
					isFirstOrder = false;
					break;
				}
			}
			if (isFirstOrder) {
				firstOrder.add(f);
			} else {
				other.add(f);
			}
		}

		secondOrder = new ArrayList<>();
		secondOrder.add(firstOrder.get(0));

		for (int i = 1; i < firstOrder.size(); i++) {
			Fraction f = firstOrder.get(i);

			boolean isSecondOrder = true;
			for (Fraction fraction : secondOrder) {
				if (fraction.getSecondOrderError().compareTo(f.getSecondOrderError()) <= 0) {
					isSecondOrder = false;
					break;
				}
			}
			if (isSecondOrder) {
				secondOrder.add(f);
			}
		}

		for (Fraction f : secondOrder) {
			firstOrder.remove(f);
		}

		removeFromList(firstOrder, n);
		removeFromList(secondOrder, n);
		removeFromList(other, n);

		other.sort(Comparator.comparing(Fraction::getFirstOrderError));
	}

	private String listToString(final List<Fraction> data) {
		return data.stream().map(elem -> elem.toString()).collect(Collectors.joining(", "));
	}
	
	public String getFirstOrderApproximations() {
		return listToString(firstOrder);
	}

	public String getSecondOrderApproximations() {
		return listToString(secondOrder);
	}

	public String getOtherApproximations() {
		return listToString(other);
	}

	private static void removeFromList(final List<Fraction> list, BigInteger lowerLimit) {
		final List<Fraction> fractions = new ArrayList<>();
		for (Fraction f : list) {
			if (f.getDenominator().compareTo(lowerLimit) < 0) {
				fractions.add(f);
			}
		}

		for (Fraction f : fractions) {
			list.remove(f);
		}
	}
}
